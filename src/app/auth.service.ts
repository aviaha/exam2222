import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs'
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import { EmailValidator } from '@angular/forms';
import * as firebase from 'firebase/app';


@Injectable({
  providedIn: 'root'
})


export class AuthService {
  
  signupEmail(email:string, password:string){
  return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password);
  }


signupGoogle(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.fireBaseAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
          })
        })
      }
updateName(user, name){
  user.updateProfile({displayName:name,photoURL:''});
  
}        
login(email,password){
return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password);
} 

loginGoogle(){
  return new Promise<any>((resolve, reject) => {
    let provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');
    this.fireBaseAuth.auth
    .signInWithPopup(provider)
    .then(res => {
      resolve(res);
    })
  })
}       
logout(){
  return this.fireBaseAuth.auth.signOut();
}                          
 //
                                             //מוסיף בדטה בייס את המילים החסרות
addUser(user, name:string,email:string){
  let uid = user.uid;                      // לדוגמא אם היה דטה בייס ריק היה ברגע ההרצה של הקוד הזה היה נוצר המילה יוסרס ומתחתיו האיי די של המשתמש שיוצר ומתחתיו השם משתמש של אותו  משתמש            
  let ref = this.db.database.ref('/');
  ref.child('users').child(uid).push({'name':name,'email':email});  
}

user:Observable<firebase.User>;
  constructor(private fireBaseAuth:AngularFireAuth, 
    private db:AngularFireDatabase) {
      this.user = fireBaseAuth.authState;
     }
}